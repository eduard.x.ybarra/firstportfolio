import React from 'react';
import { Link } from "react-router-dom";
//import './css/Home.scss';

const Home = () => {
  return (
    <div className="content">
      <div className="content__container">
        <p className="content__container__text">
          eduard ybarra
        </p>
      </div>
      <div className="_information">
        <p>
          Pursuing the latest in technology with a passionate hunger of what's next, I practice my skills to position myself in the forefront of an evergrowing industry. 
        </p>
        <Link to="/portfolio">
          <button className="_information_button">
            VIEW MY PORTFOLIO
            <i className="fas fa-long-arrow-alt-right"></i>
          </button>
        </Link>
      </div>
    </div>
  )
}

export default Home;