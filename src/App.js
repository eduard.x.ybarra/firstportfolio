import React from 'react';
import AboutMe from './paths/AboutMe.jsx';
import Contact from './paths/Contact.jsx';
import Error from './paths/Error.jsx';
import RingBling from './ringbling/Game/RingBling.jsx';
import Portfolio from './portfolio/sections/Portfolio.jsx';
import Curriculum from './curriculum/Curriculum.jsx';
import InputCalendar from './calendar/InputCalendar.jsx';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

const App = () => {

  return (
    <BrowserRouter>
      <div>
        <Switch>
          <React.Fragment>
          <Route path="/aboutme" component={AboutMe} />
          <Route path="/contact" component={Contact} />
          <Route path="/curriculum" component={Curriculum} />
          <Route path="/calendar" component={InputCalendar} />
          <div className="portfolio">
            <Route path="/" exact component={Portfolio} />
          </div>
            <div className="ringbling">
              <Route path="/ringbling" render={() => 
                <RingBling boardSize={13} playerSize={25} />
              }/>
            </div>
          </React.Fragment>
          <Route component={Error} />
        </Switch>
      </div>
    </BrowserRouter>
  )
}

export default App;
