import React from 'react';
import budatch from '../image/budatch.png';
import secondportfolio from '../image/secondportfolio.png';
import calendar from '../image/calendar.png';
import purecv from '../image/purecv.png';
import ringblinggame from '../image/ringblinggame.png';
import Popup from "reactjs-popup";
import { Link } from "react-router-dom";
import '../css/Projects.css';

const Projects = ( { project, project1, project2, project3, project4, project5, seeProject} ) => {

  return (
    <div className="cases">
      <h3>{project}</h3>
      <div className="case">
        <Popup trigger={<img src={budatch} alt="Calendar"/>}
               position="top center"
               on="hover"
        >
               Axios, Bootstrap, Cheerio, @Emailjs/Browser, ReactPlayer & Styled Components
        </Popup>
        <p>{project1}</p>
        <Link to="/budatchdesign">
          <h6>{seeProject}</h6>
        </Link>
      </div>
      <div className="case">
        <Popup trigger={<img src={secondportfolio} alt="Calendar"/>}
               position="top center"
               on="hover"
        >
               Gsap, React-Parallax-Tilt, React-Scroll & CSS
        </Popup>
        <p>{project2}</p>
        <Link to="/secondportfolio">
          <h6>{seeProject}</h6>
        </Link>
      </div>
      <div className="case">
        <Popup trigger={<img src={ringblinggame} alt="Calendar"/>}
               position="top center"
               on="hover"
        >
               React Hooks & CSS
        </Popup>
        <p>{project3}</p>
        <Link to="/ringbling">
          <h6>{seeProject}</h6>
        </Link>
      </div>
      <div className="case">
        <Popup className="popup"
               trigger={<img src={purecv} alt="PureCV"/>}
               position="top center"
               on="hover"
        >
               VanillaJS & CSS
        </Popup>
        <p>{project4}</p>
        <Link to="/curriculum">
          <h6>{seeProject}</h6>
        </Link>
      </div>
      <div className="case">
        <Popup trigger={<img src={calendar} alt="Ringbling Game"/>}
               position="top center"
               on="hover"
        >
                MomentJS / Moment-range & CSS & React Hooks
        </Popup>
        <p>{project5}</p>
        <Link to="/calendar">
          <h6>{seeProject}</h6>
        </Link>
      </div>
    </div>
  )
}

export default Projects;