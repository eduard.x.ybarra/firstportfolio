import React from 'react';
import '../css/Contact.css';

const Contact = ({ contact, sendEmail }) => {

  return (
    <div className="contact">
      <h3>{contact}</h3>
      <div>
        <a href={`mailto:eduard.x.ybarra@gmail.com`}>
          <button>{sendEmail}</button>
        </a>
        <p>+46 70-2266077</p>
      </div>
    </div>
  )
}

export default Contact;