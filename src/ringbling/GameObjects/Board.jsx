import React from 'react';
import PropTypes from 'prop-types';
import './Board.css';

const style = (dimension, color) => {
  return {
      width: dimension + 'px',
      height: dimension + 'px',
      border: '4px solid ' + color,
      borderRadius: '4px',
      position: 'relative',
      margin: '25px auto',
      overflow: 'hidden'
  }
}

const Board = ({ dimension, color, children }) => {
  
  return (
    <div className="board" style={style(dimension, color)}>
      <div className="midground">
        <div className="foreground">
          {children}
        </div>
      </div>
    </div>
  )
}

Board.propTypes = {
  dimension: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired
}

export default Board;