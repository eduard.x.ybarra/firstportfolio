import React from 'react';
import PropTypes from 'prop-types';
import './Rings.css';

const style = (size, color, top, left) => {
  return {
      width: size + 'px',
      height: size + 'px',
      position: 'absolute',
      top: top + 'px',
      left: left + 'px',
      transition: 'all 0.1s ease',
      boxShadow: '0 0 0 2px ' + color,
      borderRadius: '50%'
  }
}

const Rings = ({ size, rings }) => {
  
  return (
    rings.map(ring => {
      return (
        <div key={ring.key} className="rings" style={style(size, ring.color, ring.top, ring.left)}/>
      )
    })
  )
}

Rings.propTypes = {
  size: PropTypes.number.isRequired,
  rings: PropTypes.array.isRequired
}

export default Rings;