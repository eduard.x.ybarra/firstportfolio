import React from 'react';
import PropTypes from 'prop-types';
import redSuperman from '../images/redSuperman.png';
import yellowSuperman from '../images/yellowSuperman.png';
import greenSuperman from '../images/greenSuperman.png';
import blueSuperman from '../images/blueSuperman.png';

const style = ( top, left ) => {

  return {
      width: '40px',
      height: '25px',
      backgroundPosition: 'absolute',
      position: 'absolute',
      top: top + 'px',
      left: left + 'px'
  }
}

const Player = ({ color, top, left }) => {

  let superman;
  
  switch(color.toLowerCase()) {
    case 'yellow':
      superman = yellowSuperman;
      break;
    case 'red':
      superman = redSuperman;
      break;
    case 'blue':
      superman = blueSuperman;
      break;
    case 'green':
      superman = greenSuperman;
      break;
    default:
      break;
  }

  return (
    <img src={superman} alt="superman" style={style(top, left)} />
  )
}

Player.propTypes = {
    top: PropTypes.number.isRequired,
    left: PropTypes.number.isRequired
}

export default Player;