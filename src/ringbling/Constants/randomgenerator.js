export const rngColor = (array) => array[Math.floor(Math.random() * array.length)];
export const rngNumber = (min, max) => min + Math.floor((Math.random() * (max-min)));