import React, { useState } from 'react';
import './Text.css';

const Text = (props) => {

  const [dateText, setDateText] = useState(props.dates)

  const showTextDate = (event) => {
    event.preventDefault();
    props.showDate(dateText.date, false);
  }

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    setDateText({...dateText, [name]: value})
  }
  
  const handleOnSubmit = (event) => {
    event.preventDefault();

    props.dateTextSubmit(dateText.date, dateText)
  }

  return (
    <div className="text">
      {dateText.showDate ? (
        <form onSubmit={handleOnSubmit}>
          <p>{props.dates.date}</p>
          <textarea type="text" name="text" placeholder="Nothing Saved..." value={dateText.text} onChange={handleInputChange} cols="30" rows="5" ></textarea>
          <button>Save</button>
          <button onClick={showTextDate}>X</button>
        </form>
      ) : (
        null
      )}
    </div>
  )
}

export default Text;