import React, { useState, useEffect } from 'react';
import Moment from 'moment';
import { extendMoment } from 'moment-range';
import { Row, Col } from 'react-bootstrap';
import DateInput from './DateInput.jsx';
import './Calendar.css';

const Calendar = ({ handleDateText }) => {

  const [date, setDate] = useState(extendMoment(Moment));
  const [selectedDate, setSelectedDate] = useState(date.format('DD-MM-YYYY'))

  useEffect(() => {
    document.title = `${Moment().format('DD-MM-YYYY')}`
  })

  const handleDate = (date) => {
    const dateText = {date: date, text: '', showDate: true}; //send it with a boolean (show date with text or)
    handleDateText(dateText);
  }

  const _handleSubmitValidation = (date) => {
    const typedDate = Moment(date, 'DD-MM-YYYY')
    if(typedDate.isValid()) {
      setDate(Moment(date, 'DD-MM-YYYY'))
      setSelectedDate(date)
    } else {
      window.alert(date + ' is not a valid date!')
    }
  }

  const displayMonthButtons = () => {
    const prevMonth = Moment(date).subtract(1, 'month').format('MMM');
    const rightArrow = '>'
    const nextMonth = Moment(date).add(1, 'month').format('MMM');
    const leftArrow = '<'

    return (
      <div className="month">
        <button className="previous" onClick={() => setDate(Moment(date).subtract(1, 'month'))}>{leftArrow} {prevMonth}</button>
        <button className="current" onClick={() => setDate(Moment())}>Today</button>
        <button className="next" onClick={() => setDate(Moment(date).add(1, 'month'))}>{nextMonth} {rightArrow}</button>
      </div>
    )
  }

  const displayCurrentDate = () => {
    const searchedDate = date.format('DD-MM-YYYY')

    return (
      <DateInput searchedDate={searchedDate} handleSubmitValidation={_handleSubmitValidation}/>
    )
  }

  const displayDays = () => {
    const weekdays = [];
    const cells = [];
    let previousMonthDays = 0;
    let nextMonthDays = 0;
    const currentDates = [];
    const startOfMonth = date.clone().startOf('month').format('YYYY-MM-DD'); // 'DD-MM-YYYY' not supported
    const endOfMonth = date.clone().endOf('month').format('YYYY-MM-DD');
    const ranged = date.range(startOfMonth, endOfMonth);
    const days = ranged.by('days');

    const highlightStyle = {
      color: 'BlueViolet',
      background: '#e1e1e1'
    };

    [...days].map((date) => 
      currentDates.push(
        date.format('DD-MM-YYYY')
      )
    )

    for(let i = 1; i <= 7; i++) {
      weekdays.push(
        Moment().isoWeekday(i).format('ddd')
      )
    }
    
    for(let i = 0; i < 7; i++) {
      const startPosition = Moment(startOfMonth).format('ddd');
      if(weekdays[i] === startPosition) {
        previousMonthDays = i;
      }
    }

    for(let i = 0; i < 6; i++) {
      const columns = []
      for(let j = 0; j < 7; j++) {
        if(i === 0 && j < previousMonthDays) {
          const previousMonthDates = Moment(startOfMonth).subtract(-j + previousMonthDays, 'day').format('DD-MM-YYYY');
          let previousDays= previousMonthDates.substr(0, previousMonthDates.indexOf('-'));
          if(previousDays.charAt(0) === '0') {
            previousDays = previousDays.substr(1);
          }
          columns.push(
            <Col className="columns-prev" style={selectedDate === previousMonthDates ? highlightStyle : null} key={previousMonthDates} onClick={() => setSelectedDate(previousMonthDates)} onDoubleClick={() => handleDate(previousMonthDates)}>
              <span>{previousDays}</span>
            </Col>
          )
        } else {
          if(currentDates === undefined || currentDates.length === 0) {
            nextMonthDays = nextMonthDays + 1;
            const nextMonthDates = Moment(endOfMonth).add(nextMonthDays, 'day').format('DD-MM-YYYY');
            let nextDays= nextMonthDates.substr(0, nextMonthDates.indexOf('-'));
            if(nextDays.charAt(0) === '0') {
              nextDays = nextDays.substr(1);
            }
            columns.push(
              <Col className="columns-next" style={selectedDate === nextMonthDates ? highlightStyle : null} key={nextMonthDates} onClick={() => setSelectedDate(nextMonthDates)} onDoubleClick={() => handleDate(nextMonthDates)}>
                <span>{nextDays}</span>
              </Col>
            )
          } else {
            const currentMonthDates = currentDates[0];
            let currentDays= currentMonthDates.substr(0, currentMonthDates.indexOf('-'));
            if(currentDays.charAt(0) === '0') {
              currentDays = currentDays.substr(1);
            }
            columns.push(
              <Col className="columns" style={selectedDate === currentMonthDates ? highlightStyle : null} key={currentMonthDates} onClick={() => setSelectedDate(currentMonthDates)} onDoubleClick={() => handleDate(currentMonthDates)}>
                <span>{currentDays}</span>
              </Col>
            )
            currentDates.splice(0, 1);
          }
        }
      }
      cells.push(
        <Row className="cells" key={i}>
          {columns}
        </Row>
      )
    }

    return (
      <div>
        <h3>{date.format('MMMM')} {date.format('YYYY')}</h3>
        <Row className="weekdays-header">
          {weekdays.map((day, index) => (
            <Col className="weekdays" key={index}>{day}</Col>
          ))}
        </Row>
        {cells}
      </div>
    )
  }

  return (
    <div className="calendar">
      <div>
        {displayMonthButtons()}
      </div>
      <div>
        {displayCurrentDate()}
      </div>
      <div>
        {displayDays()}
      </div>
    </div>
  )
}

export default Calendar;