import React, { useState, useEffect } from 'react';
import './DateInput.css';

const DateInput = (props) => {

  const[date, setDate] = useState('');

  useEffect(() => {
    if(date.length === 10) {
      document.getElementById('button2').focus();
    }
  })

  const handleInputChange = (event) => {
    const regex = /^[0-9-]+$/;
    let newDate = event.target.value;
    let dateLength = newDate.length;

    if(!regex.test(newDate)) return

    if(dateLength > 10) return

    if(newDate.charAt(0) > 3) {
      return
    }
    
    if(newDate.charAt(0) === '3') {
      if(newDate.charAt(1) > 1) {
        return
      }
    }

    if(newDate.charAt(0) === '0') {
      if(newDate.charAt(1) === '0') {
        return
      }
    }
    
    if(newDate.charAt(3) === '0') {
      if(newDate.charAt(4) === '0') {
        return
      }
    }
    
    if(newDate.charAt(3) > 1) {
      return
    }

    if(newDate.charAt(3) === '1') {
      if(newDate.charAt(4) > '2') {
        return
      }
    }

    if(dateLength === 2 || dateLength === 5) {
      newDate = newDate + '-';
    }

    setDate(newDate);
  }

  const handleOnSubmit = (event) => {
    event.preventDefault();
    const regexrDotcom = /(\d\d?-){2}\d\d\d\d/g; // Unnecessary escape character: \-  no-useless-escape
    const valid = regexrDotcom.test(date);
    if(valid) {
      props.handleSubmitValidation(date);
      setDate('');
    } else {
      window.alert('Not valid!')
      setDate('');
    }
  }

  const onResetClick = (event) => {
    event.preventDefault();
    setDate('');
  }

  return (
    <form className="date-form" onSubmit={handleOnSubmit}>
      <button className="reset" type="submit" onClick={onResetClick} >Reset</button>
      <input  className="input" type="text" placeholder={props.searchedDate} onChange={handleInputChange} value={date} required></input>
      <button id="button2" className="submit" type="submit">Find Date</button>
    </form>
  )
}

export default DateInput;