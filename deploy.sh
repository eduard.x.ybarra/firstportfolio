#!/bin/bash

echo 'building react project'
npm run build

echo 'transfering ..'
scp -r -P 2218 -i ~/.LocalKeys/RaspberryPi/RaspberryPi build pi@eduardybarra.com:/tmp

echo 'copying to /var/www'
ssh -p 2218 -i ~/.LocalKeys/RaspberryPi/RaspberryPi pi@eduardybarra.com 'sudo rm -r /var/www/html;sudo mv /tmp/build /var/www/html'

echo 'done!'